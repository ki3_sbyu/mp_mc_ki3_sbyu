const byte piezoPin = 2; 
const byte COUNT_NOTES = 39; 

void playNote(char tonePin, float freq, int time)
{
  float per = 1.00/(2.00*freq);
  int soundTime = millis();
  while (millis() - soundTime < time) {
  	digitalWrite(tonePin, LOW);
  	delayMicroseconds(per * 1000000);
  	digitalWrite(tonePin, HIGH);
  	delayMicroseconds(per * 1000000);
  }
}

int tones[COUNT_NOTES] = { 392, 392, 392, 311, 466, 392, 311, 466, 392, 587, 587, 587, 622, 466, 369, 311, 466, 392, 784, 392, 392, 784, 739, 698, 659, 622, 659, 415, 554, 523, 493, 466, 440, 466, 311, 369, 311, 466, 392 }; 

// длительности нот 
int durations[COUNT_NOTES] = { 350, 350, 350, 250, 100, 350, 250, 100, 700, 350, 350, 350, 250, 100, 350, 250, 100, 700, 350, 250, 100, 350, 250, 100, 100, 100, 450, 150, 350, 250, 100, 100, 100, 450, 150, 350, 250, 100, 750 }; 

void setup() { 
  pinMode(piezoPin, OUTPUT); // настраиваем вывод 2 на выход 
} 
void loop() { 
  for (int i = 0; i <= COUNT_NOTES; i++) { 
    playNote(piezoPin, tones[i]*3, durations[i] * 2); 
    delay(durations[i] * 2); 
    noTone(piezoPin); 
  } 
}
