int displayPins[] = {
    7,
    8,
    9,
    10,
    11,
    12,
    13
};
int zero[] = {
    7,
    8,
    9,
    11,
    12,
    13
};
int one[] = {
    7,
    13
};
int two[] = {
    8,
    9,
    10,
    12,
    13
};
int three[] = {
    7,
    8,
    10,
    12,
    13
};
int four[] = {
    7,
    10,
    11,
    13
};
int five[] = {
    7,
    8,
    10,
    11,
    12
};
int six[] = {
    7,
    8,
    9,
    10,
    11,
    12
};
int seven[] = {
    7,
    12,
    13
};
int eight[] = {
    7,
    8,
    9,
    10,
    11,
    12,
    13
};
int nine[] = {
    7,
    8,
    10,
    11,
    12,
    13
};
void setup() {
    for (int i = 0; i < 7; i ++) 
        pinMode(displayPins[i], OUTPUT);
    
}
void loop() {
    for (int i = 0; i < 7; i ++) 
        digitalWrite(displayPins[i], HIGH);
    
    for (int i = 0; i < 10; i ++) {
        Display(i);
        delay(1000);
        for (int i = 0; i < 7; i ++) 
            digitalWrite(displayPins[i], HIGH);
        
    }
}
void Display(int number) {
    if (number == 0) 
        for (int i = 0; i < (sizeof(zero) / sizeof(int)); i ++) 
            digitalWrite(zero[i], LOW);
        
    
    if (number == 1) 
        for (int i = 0; i < (sizeof(one) / sizeof(int)); i ++) 
            digitalWrite(one[i], LOW);
        
    
    if (number == 2) 
        for (int i = 0; i < (sizeof(two) / sizeof(int)); i ++) 
            digitalWrite(two[i], LOW);
        
    
    if (number == 3) 
        for (int i = 0; i < (sizeof(three) / sizeof(int)); i ++) 
            digitalWrite(three[i], LOW);
        
    
    if (number == 4) 
        for (int i = 0; i < (sizeof(four) / sizeof(int)); i ++) 
            digitalWrite(four[i], LOW);
        
    
    if (number == 5) 
        for (int i = 0; i < (sizeof(five) / sizeof(int)); i ++) 
            digitalWrite(five[i], LOW);
        
    
    if (number == 6) 
        for (int i = 0; i < (sizeof(six) / sizeof(int)); i ++) 
            digitalWrite(six[i], LOW);
        
    
    if (number == 7) 
        for (int i = 0; i < (sizeof(seven) / sizeof(int)); i ++) 
            digitalWrite(seven[i], LOW);
        
    
    if (number == 8) 
        for (int i = 0; i < (sizeof(eight) / sizeof(int)); i ++) 
            digitalWrite(eight[i], LOW);
        
    
    if (number == 9) 
        for (int i = 0; i < (sizeof(nine) / sizeof(int)); i ++) 
            digitalWrite(nine[i], LOW);
        
    
}
