#define STATE_DELAY 1000

char x1 = 0;
char x2 = 0;
char x3 = 0;
char x4 = 0;


int ledPin13 = LED_BUILTIN;

void setup() {
  pinMode(ledPin13, OUTPUT);
}

void loop() {
  bool d1 = !(x3 && !x4 || !(x1 && x2) && x4 || !(x2 && x3) && x4 || !(x1 && x4) && x2);

  digitalWrite(ledPin13, d1);
}
