int displayPinsIndOne[] = {
    7,
    8,
    9,
    10,
    11,
    12,
    13
};
char displayPinsIndTwo[] = {
    5,
    'A0',
    'A1',
    'A2',
    'A3',
    'A4',
    'A5'
};
int zero[] = {
    7,
    8,
    9,
    11,
    12,
    13
};
int one[] = {
    7,
    13
};
int two[] = {
    8,
    9,
    10,
    12,
    13
};
int three[] = {
    7,
    8,
    10,
    12,
    13
};
int four[] = {
    7,
    10,
    11,
    13
};
int five[] = {
    7,
    8,
    10,
    11,
    12
};
int six[] = {
    7,
    8,
    9,
    10,
    11,
    12
};
int seven[] = {
    7,
    12,
    13
};
int eight[] = {
    7,
    8,
    9,
    10,
    11,
    12,
    13
};
int nine[] = {
    7,
    8,
    10,
    11,
    12,
    13
};
char Zero[] = {
    5,
    A0,
    A1,
    A3,
    A4,
    A5
};
char One[] = {
    5,
    A5
};
char Two[] = {
    A0,
    A1,
    A2,
    A4,
    A5
};
char Three[] = {
    5,
    A0,
    A2,
    A4,
    A5
};
char Four[] = {
    5,
    A2,
    A3,
    A5
};
char Five[] = {
    5,
    A0,
    A2,
    A3,
    A4
};
char Six[] = {
    5,
    A0,
    A1,
    A2,
    A3,
    A4
};
char Seven[] = {
    5,
    A4,
    A5
};
char Eight[] = {
    5,
    A0,
    A1,
    A2,
    A3,
    A4,
    A5
};
char Nine[] = {
    5,
    A0,
    A2,
    A3,
    A4,
    A5
};
int butPress = 0;
int v,
sound = 0;
int numOfSeconds = 66;

void setup() {
    for (int i = 0; i < 7; i ++) {
        pinMode(displayPinsIndOne[i], OUTPUT);
        pinMode(displayPinsIndTwo[i], OUTPUT);
    }
    pinMode(6, OUTPUT);
}
void loop() {
    for (int i = 0; i < 7; i ++) {
        digitalWrite(displayPinsIndOne[i], HIGH);
        digitalWrite(displayPinsIndTwo[i], HIGH);
    }
    v = digitalRead(4);
    if (v == HIGH) 
        butPress = 1;
    
    if (butPress == 1) {
        if (sound == 1) {
            digitalWrite(6, LOW);
            delayMicroseconds(500);
            digitalWrite(6, HIGH);
            delayMicroseconds(500);
        } else {
            int numOne = numOfSeconds / 10;
            int numTwo = numOfSeconds % 10;
            for (int i = numOne; i >= 0; i --) {
                for (int j = numTwo; j >= 0; j --) {
                    Display(i, j);
                    delay(1000);
                    if (i == 0 && j == 0) {
                        sound = 1;
                        break;
                    }
                    for (int k = 0; k < 7; k ++) {
                        digitalWrite(displayPinsIndOne[k], HIGH);
                        digitalWrite(displayPinsIndTwo[k], HIGH);
                    }
                }
                if (sound == 1) 
                    break;
                
                numTwo = 9;
            }
        }
    }
}
void Display(int nOne, int nTwo) {
    if (nOne == 0) 
        for (int i = 0; i < (sizeof(zero) / sizeof(int)); i ++) 
            digitalWrite(zero[i], LOW);
        
    
    if (nOne == 1) 
        for (int i = 0; i < (sizeof(one) / sizeof(int)); i ++) 
            digitalWrite(one[i], LOW);
        
    
    if (nOne == 2) 
        for (int i = 0; i < (sizeof(two) / sizeof(int)); i ++) 
            digitalWrite(two[i], LOW);
        
    
    if (nOne == 3) 
        for (int i = 0; i < (sizeof(three) / sizeof(int)); i ++) 
            digitalWrite(three[i], LOW);
        
    
    if (nOne == 4) 
        for (int i = 0; i < (sizeof(four) / sizeof(int)); i ++) 
            digitalWrite(four[i], LOW);
        
    
    if (nOne == 5) 
        for (int i = 0; i < (sizeof(five) / sizeof(int)); i ++) 
            digitalWrite(five[i], LOW);
        
    
    if (nOne == 6) 
        for (int i = 0; i < (sizeof(six) / sizeof(int)); i ++) 
            digitalWrite(six[i], LOW);
        
    
    if (nOne == 7) 
        for (int i = 0; i < (sizeof(seven) / sizeof(int)); i ++) 
            digitalWrite(seven[i], LOW);
        
    
    if (nOne == 8) 
        for (int i = 0; i < (sizeof(eight) / sizeof(int)); i ++) 
            digitalWrite(eight[i], LOW);
        
    
    if (nOne == 9) 
        for (int i = 0; i < (sizeof(nine) / sizeof(int)); i ++) 
            digitalWrite(nine[i], LOW);
        
    
    if (nTwo == 0) 
        for (int j = 0; j < 6; j ++) 
            digitalWrite(Zero[j], LOW);
        
    
    if (nTwo == 1) 
        for (int j = 0; j < 2; j ++) 
            digitalWrite(One[j], LOW);
        
    
    if (nTwo == 2) 
        for (int j = 0; j < 5; j ++) 
            digitalWrite(Two[j], LOW);
        
    
    if (nTwo == 3) 
        for (int j = 0; j < 5; j ++) 
            digitalWrite(Three[j], LOW);
        
    
    if (nTwo == 4) 
        for (int j = 0; j < 4; j ++) 
            digitalWrite(Four[j], LOW);
        
    
    if (nTwo == 5) 
        for (int j = 0; j < 5; j ++) 
            digitalWrite(Five[j], LOW);
        
    
    if (nTwo == 6) 
        for (int j = 0; j < 6; j ++) 
            digitalWrite(Six[j], LOW);
        
    
    if (nTwo == 7) 
        for (int j = 0; j < 3; j ++) 
            digitalWrite(Seven[j], LOW);
        
    
    if (nTwo == 8) 
        for (int j = 0; j < 7; j ++) 
            digitalWrite(Eight[j], LOW);
        
    
    if (nTwo == 9) 
        for (int j = 0; j < 6; j ++) 
            digitalWrite(Nine[j], LOW);
        
    
}
